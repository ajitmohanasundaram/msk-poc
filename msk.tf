##################################################################################
# PROVIDERS
##################################################################################

provider "aws" {
  access_key = ""
  secret_key = ""
  region     = "us-east-2"
}

resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = "true"
}

data "aws_availability_zones" "azs" {
  state = "available"
}

resource "aws_subnet" "subnet_az1" {
  availability_zone = data.aws_availability_zones.azs.names[0]
  cidr_block        = "10.0.0.0/24"
  vpc_id            = aws_vpc.vpc.id
}

resource "aws_subnet" "subnet_az2" {
  availability_zone = data.aws_availability_zones.azs.names[1]
  cidr_block        = "10.0.1.0/24"
  vpc_id            = aws_vpc.vpc.id
}
resource "aws_security_group" "sg-ec2" {
  name = "allow-all-sg-ec2"
  vpc_id = aws_vpc.vpc.id
  description = "security group for ec2/ client egress"
  // Terraform removes the default rule
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "sg2" {
  name = "allow-all-sg-msk"
  vpc_id = aws_vpc.vpc.id
  description = "security group for msk and ec2"
}
/*
resource "aws_security_group_rule" "ingress_sshport" {
  y_type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_securitgroup.sg2.id
}*/
resource "aws_security_group_rule" "ingress_kafka" {
  type              = "ingress"
  from_port         = 9094
  to_port           = 9094
  protocol          = "tcp"
  cidr_blocks = ["10.0.0.0/24","10.0.1.0/24"]
  security_group_id = aws_security_group.sg2.id
}
resource "aws_security_group_rule" "ingress_zookeeper" {
  type              = "ingress"
  from_port         = 2181
  to_port           = 2181
  protocol          = "tcp"
  cidr_blocks = ["10.0.0.0/24","10.0.1.0/24"]
  security_group_id = aws_security_group.sg2.id
}

resource "aws_route_table" "rtb" {
  vpc_id = aws_vpc.vpc.id

  route {
    gateway_id = aws_internet_gateway.igw.id
    cidr_block = "0.0.0.0/0"
  }
}

resource "aws_route_table_association" "rta-subnet_az1" {
  subnet_id      = aws_subnet.subnet_az1.id
  route_table_id = aws_route_table.rtb.id
}

data "aws_ssm_parameter" "ami" {
  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}


resource "aws_instance" "msk_ec2_instance" {
  ami                    = nonsensitive(data.aws_ssm_parameter.ami.value)
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.subnet_az1.id
  vpc_security_group_ids = [aws_security_group.sg-ec2.id]

 # provisioner "remote-exec" {
  #  inline = [
   #   "sudo yum install java-1.8.0",
    #  "wget https://archive.apache.org/dist/kafka/2.6.2/kafka_2.12-2.6.2.tgz",
     # "tar -xzf kafka_2.12-2.6.2.tgz"
    #]
  #}

}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

}
resource "aws_eip" "ip-demo-env" {
  instance = aws_instance.msk_ec2_instance.id
  vpc      = true
}

resource "aws_kms_key" "kms" {
  description = "aws_kms_key_demo"
}

resource "aws_cloudwatch_log_group" "msk_broker_cloudwatch_logs" {
  name = "msk_broker_logs"
}

resource "aws_s3_bucket" "bucket" {
  bucket = "msk-broker-logs-bucket-ajit-pm-321"
  acl    = "private"
}

resource "aws_iam_role" "firehose_role" {
  name = "firehose_test_role"

  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
  {
    "Action": "sts:AssumeRole",
    "Principal": {
      "Service": "firehose.amazonaws.com"
    },
    "Effect": "Allow",
    "Sid": ""
  }
  ]
}
EOF
}

resource "aws_kinesis_firehose_delivery_stream" "test_stream" {
  name        = "terraform-kinesis-firehose-msk-broker-logs-stream"
  destination = "s3"

  s3_configuration {
    role_arn   = aws_iam_role.firehose_role.arn
    bucket_arn = aws_s3_bucket.bucket.arn
  }

  tags = {
    LogDeliveryEnabled = "placeholder"
  }

  lifecycle {
    ignore_changes = [
      tags["LogDeliveryEnabled"],
    ]
  }
}

resource "aws_msk_cluster" "demo-msk-cluster" {
  cluster_name           = "demo-msk-cluster"
  kafka_version          = "2.7.1"
  number_of_broker_nodes = 2

  broker_node_group_info {
    instance_type   = "kafka.t3.small"
    ebs_volume_size = 5
    client_subnets = [
      aws_subnet.subnet_az1.id,
      aws_subnet.subnet_az2.id]
    security_groups = [aws_security_group.sg2.id]
  }

  encryption_info {
    encryption_at_rest_kms_key_arn = aws_kms_key.kms.arn
  }

  open_monitoring {
    prometheus {
      jmx_exporter {
        enabled_in_broker = true
      }
      node_exporter {
        enabled_in_broker = true
      }
    }
  }

  logging_info {
    broker_logs {
      cloudwatch_logs {
        enabled   = true
        log_group = aws_cloudwatch_log_group.msk_broker_cloudwatch_logs.name
      }
      firehose {
        enabled         = true
        delivery_stream = aws_kinesis_firehose_delivery_stream.test_stream.name
      }
      s3 {
        enabled = true
        bucket  = aws_s3_bucket.bucket.id
        prefix  = "logs/msk-"
      }
    }
  }

  tags = {
    foo = "bar"
  }
}

output "zookeeper_connect_string" {
  value = aws_msk_cluster.demo-msk-cluster.zookeeper_connect_string
}

output "bootstrap_brokers_tls" {
  description = "TLS connection host:port pairs"
  value       = aws_msk_cluster.demo-msk-cluster.bootstrap_brokers_tls
}
